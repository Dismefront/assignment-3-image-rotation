#ifndef ROTATE_H
#define ROTATE_H
#define SIGNATURE 0x4D42
#define RESERVED 0
#define OFFSET 54
#define SIZE 40
#define PLANES 1
#define BIT_COUNT 24
#define COMPRESSION 0
#define PELS_PER_METER 0
#define CLR_USED 0
#define CLR_IMPORTANT 0

#include "bmp.h"

struct image rotate(struct image const source);

struct bmp_header create_header(struct image const *img);

#endif
