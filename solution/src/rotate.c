#include "rotate.h"
#include <stdlib.h>

struct bmp_header create_header(struct image const *img) {
    uint32_t size_image = (img->width * sizeof(struct pixel)) * img->height;
    return (struct bmp_header) {
        .bfType = SIGNATURE,
        .bfReserved = RESERVED,
        .bOffBits = OFFSET,
        .biSize = SIZE,
        .biWidth = img->width,
        .biHeight = img->height,
        .biPlanes = PLANES,
        .biBitCount = BIT_COUNT,
        .biCompression = COMPRESSION,
        .biSizeImage = size_image,
        .bfileSize = size_image + OFFSET,
        .biXPelsPerMeter = PELS_PER_METER,
        .biYPelsPerMeter = PELS_PER_METER,
        .biClrUsed = CLR_USED,
        .biClrImportant = CLR_IMPORTANT
    };
}

struct image rotate(struct image source) {
    struct image rotated = {
        .height = source.width,
        .width = source.height,
        .data = malloc(sizeof(struct pixel) * source.height * source.width)
    };

    for (size_t i = 0; i < rotated.height; i++) {
        for (size_t j = 0; j < rotated.width; j++) {
            rotated.data[j + i * rotated.width] 
                = source.data[(source.height - j - 1) * source.width + i];
        }
    }
    return rotated;
}
