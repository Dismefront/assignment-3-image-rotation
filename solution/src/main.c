#include "../include/bmp.h"
#include "../include/rotate.h"
#include "../include/status.h"

#include <inttypes.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ARGUMENT_ERROR 1

int main(int argc, char** argv) {
    size_t read_arg = 0;

    if (argc < 3) {
        printf("less than 3 arguments %d", argc);
        return ARGUMENT_ERROR;
    }

    FILE* stream = fopen(argv[1], "rb");

    if (stream == NULL) {
        printf("Could not read file...");
        return ARGUMENT_ERROR;
    }

    struct bmp_header header = { 0 };
    read_arg = fread(&header, sizeof(struct bmp_header), 1, stream);
    if (read_arg == 0) {
        return -1;
    }

    struct pixel* pixels = malloc(header.biWidth * header.biHeight * sizeof(struct pixel));
    struct image img = { header.biWidth, header.biHeight, pixels };
    read_arg = from_bmp(stream, &img);
    if (read_arg != READ_OK) {
        return -1;
    }

    FILE* out = fopen(argv[2], "wb");
    struct image rotated = rotate(img);
    if (to_bmp(out, &rotated) != WRITE_OK)
        return -1;

    free(pixels);
    fclose(stream);
    fclose(out);
    free(rotated.data);

    printf("ROTATED SUCCESSFULLY");

    return 0;
}
