#include "bmp.h"

struct bmp_header create_header_rotated(struct bmp_header const h) {
    return (struct bmp_header) {
        .bfType = h.bfType,
            .bfReserved = h.bfileSize,
            .bOffBits = h.bOffBits,
            .biSize = h.biSize,
            .biWidth = h.biWidth,
            .biHeight = h.biHeight,
            .biPlanes = h.biPlanes,
            .biBitCount = h.biBitCount,
            .biCompression = h.biCompression,
            .biSizeImage = h.biSizeImage,
            .bfileSize = h.bfileSize,
            .biXPelsPerMeter = h.biXPelsPerMeter,
            .biYPelsPerMeter = h.biYPelsPerMeter,
            .biClrUsed = h.biClrUsed,
            .biClrImportant = h.biClrImportant
    };
}
