#include "bmp.h"
#include "rotate.h"
#include "status.h"
#include <stdio.h>

enum read_status from_bmp(FILE* in, struct image* img) {
	long padding = (long) (4 - img->width * sizeof(struct pixel) % 4) % 4;

    for (size_t i = 0; i < img->height; i++) {
        if (!fread(img->data + i * img->width, sizeof(struct pixel) * img->width, 1, in))
            return READ_ERROR;
        fseek(in, padding, SEEK_CUR);
    }
    return READ_OK;
}

enum write_status to_bmp(FILE* out, struct image const* img) {
    long padding = (long) (4 - img->width * sizeof(struct pixel) % 4) % 4;

    struct bmp_header new_header = create_header(img);
    if (!fwrite(&new_header, sizeof(struct bmp_header), 1, out))
        return WRITE_ERROR;
    for (size_t i = 0; i < img->height; i++) {
        fwrite(img->data + i * img->width, sizeof(struct pixel), img->width, out);
        fwrite("\0\0\0", padding, 1, out);
    }
    return WRITE_OK;
}
